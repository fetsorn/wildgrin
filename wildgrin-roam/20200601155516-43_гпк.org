#+TITLE: 43 ГПК
ГПК РФ Статья 43. Третьи лица, не заявляющие самостоятельных требований относительно предмета спора

""1. Третьи лица, не заявляющие самостоятельных требований относительно предмета спора, могут вступить в дело на стороне истца или ответчика до принятия судом первой инстанции судебного постановления по делу, если оно может повлиять на их права или обязанности по отношению к одной из сторон. Они могут быть привлечены к участию в деле также по ходатайству лиц, участвующих в деле, или по инициативе суда. Третьи лица, не заявляющие самостоятельных требований относительно предмета спора, пользуются процессуальными правами и несут процессуальные обязанности стороны, за исключением права на изменение основания или предмета иска, увеличение или уменьшение размера исковых требований, отказ от иска, признание иска, а также на предъявление встречного иска и требование принудительного исполнения решения суда.
(в ред. Федерального закона от 26.07.2019 N 197-ФЗ)
(см. текст в предыдущей "редакции")
При этом третьи лица, не заявляющие самостоятельных требований относительно предмета спора, вправе выступать участниками мирового соглашения в случаях, если они приобретают права либо на них возлагается обязанность по условиям данного соглашения.
(абзац введен Федеральным законом от 26.07.2019 N 197-ФЗ)
О вступлении в дело третьих лиц, не заявляющих самостоятельных требований относительно предмета спора, выносится определение суда.
""2. При вступлении в процесс третьего лица, не заявляющего самостоятельных требований относительно предмета спора, рассмотрение дела в суде производится с самого начала.
